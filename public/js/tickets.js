$(document).ready(function(){

    $('.btn-update-ticket').click(function(){
        var btn     = $(this);
        var params  = {
                _token : $('meta[name="csrf-token"]').attr('content'),
                status : btn.data('status'),
                action : 'ajax'
        };

        $.ajax({
            url         : btn.data('url'),
            method      : 'put',
            dataType    : 'json',
            data        : params,
            success : function(result){
                btn.parent().parent().parent().addClass('table-success');
                setTimeout(function(){
                    btn.parent().parent().parent().fadeOut();
                },2000);
            }
        });
     });
});