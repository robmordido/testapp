@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    @if(request()->get('status') == 'closed')
                        Closed Tickets &nbsp; 
                        <a href="{{route('tickets.index')}}" class="btn btn-default btn-sm" style="float:right;"><i class="fa fa-list"></i> Show Open Tickets</a>
                    @else
                        Open Tickets &nbsp; 
                        <a href="{{route('tickets.index',['status'=>'closed'])}}" class="btn btn-default btn-sm" style="float:right;"><i class="fa fa-tasks"></i> Show Closed Tickets</a>
                    @endif
                    <a href="{{route('tickets.create')}}" class="btn btn-default btn-sm" style="float:right;"><i class="fa fa-plus-square"></i> Create Ticket</a>
                </div>

                <div class="card-body">
                   @if($tickets->count() > 0)
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <td style="width: 5%">ID</td>
                                    <td style="width: 25%">Title</td>
                                    <td style="width: 50%">Description</td>
                                    <td style="width: 5%">Status</td>
                                    <td style="width: 15%">Action</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tickets as $ticket)
                                    <tr>
                                        <td>{{$ticket->id}}</td>
                                        <td>{{$ticket->title}}</td>
                                        <td>{{$ticket->description}}</td>
                                        <td>{{$ticket->status}}</td>
                                        <td>
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                            @if($ticket->status == 'open')
                                                <a href="{{route('tickets.edit',['id'=>$ticket->id])}}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                            @endif
                                            <a href="javascript:;" data-status="{{($ticket->status == 'open') ? 'closed' : 'open'}}" data-url="{{route('tickets.update',['ticket'=>$ticket])}}" class="btn btn-success btn-sm btn-update-ticket"><i class="fa fa-check"></i> Mark as {{($ticket->status == 'open') ? 'Closed' : 'Open'}}</a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                   @else
                        <p>You have no existing open tickets.<br/><a href="{{route('tickets.create')}}">Create new ticket.</a></p>
                   @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
