@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Create New Ticket &nbsp; <a href="{{route('tickets.index')}}" class="btn btn-default btn-sm" style="float: right;"><i class="fa fa-tasks"></i> Manage Tickets</a>
                </div>

                <div class="card-body">
                   <form method="post" action="{{route('tickets.store')}}" class="form form-striped">
                    @csrf
                      <div class="form-group">
                        <label for="title">Title</label>
                        <input class="form-control" id="title" name="title" aria-describedby="emailHelp" value="{{old('title')}}">
                        @if ($errors->has('title'))
                            <span class="invalid-feedback" role="alert" style="display: block;">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                      </div>
                      <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" id="description" name="description">{{old('description')}}</textarea>
                        @if ($errors->has('description'))
                            <span class="invalid-feedback" role="alert" style="display: block;">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                      </div>

                      <button type="submit" class="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
