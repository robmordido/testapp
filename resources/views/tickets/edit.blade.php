@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Edit Ticket #{{$ticket->id}}&nbsp; <a href="{{route('tickets.index')}}" class="btn btn-default btn-sm" style="float: right;"><i class="fa fa-tasks"></i> Manage Tickets</a>
                </div>

                <div class="card-body">
                   <form id="editTicketForm" method="post" action="{{route('tickets.update',['ticket'=>$ticket])}}" class="form form-striped">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                      <div class="form-group">
                        <label for="title">Title</label>
                        <input class="form-control" id="title" name="title" aria-describedby="emailHelp" value="{{old('title') ?: $ticket->title}}">
                        @if ($errors->has('title'))
                            <span class="invalid-feedback" role="alert" style="display: block;">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                      </div>
                      <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" id="description" name="description">{{old('title') ?: $ticket->description}}</textarea>
                        @if ($errors->has('description'))
                            <span class="invalid-feedback" role="alert" style="display: block;">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                      </div>

                      <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
