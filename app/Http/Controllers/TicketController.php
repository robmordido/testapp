<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Ticket;
use App\Http\Requests\NewTicketRequest;

class TicketController extends Controller
{
    public function __construct()
    {

    }

    public function index(Request $request)
    {
    	$status     = ($request->get('status') != '') ? $request->get('status') : 'open'; 
        $tickets    = Auth::user()->tickets()->whereStatus($status)->get();
        return view('tickets.index',compact('tickets'));
    }

    public function create(Request $request)
    {
    	return view('tickets.create');
    }

    public function edit($ticket,Request $request)
    {
    	return view('tickets.edit',compact('ticket'));
    }

    public function store(NewTicketRequest $request)
    {  
    	$ticket                = new Ticket;
        $ticket->title         = $request->get('title');
        $ticket->description   = $request->get('description');
        $ticket->status        = 'open';
        $ticket->user_id       = Auth::user()->id;
        $ticket->save();
        return redirect()->route('tickets.index');
    }

    public function update($ticket,NewTicketRequest $request)
    {
        $action                 = $request->get('action');
    	$ticket->title          = $request->get('title') ?: $ticket->title;
        $ticket->description    = $request->get('description') ?: $ticket->description;
        $ticket->status         = $request->get('status') ?: $ticket->status;
        return ($action == 'ajax') ? response()->json(['success'=>$ticket->save(),'data'=>$ticket]) : redirect()->route('tickets.index');
    }
}
