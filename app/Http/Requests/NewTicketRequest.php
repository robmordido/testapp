<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class NewTicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $ticket = $this->route('ticket');
        return (!is_null($ticket) && $ticket->user_id == Auth::user()->id) ?: true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ticket = $this->route('ticket');
        return [            
            'title'         => (is_null($ticket)) ? 'required' : '',
            'description'   => (is_null($ticket)) ? 'required' : ''
        ];
    }
}
